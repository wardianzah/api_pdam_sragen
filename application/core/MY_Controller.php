<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  ===========================================================
  ::  => Author       : Robby Adnan F.
  => Email        : bobbyadnan17@gmail.com
  => Description  : RAF Controllers
  ===========================================================
 */

class RAF_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->data = array();

        $this->get_data();
    }

    public function get_data() {
        $raw_msg = file_get_contents('php://input');
        $this->data['request'] = json_decode($raw_msg);
    }

    public function get_produk_model($kode) {
        $list = array(
            array('id_produk' => 'WASRAGEN', 'processor' => 'pdam_sragen'),
        );

        $arrayKey = $this->searchArrayKeyVal("id_produk", $kode, $list);
        if ($arrayKey !== false) {
            return $list[$arrayKey]['processor'];
        } else {
            return false;
        }
    }

    protected function searchArrayKeyVal($sKey, $id, $array) {
        foreach ($array as $key => $val) {
            if ($val[$sKey] == $id) {
                return $key;
            }
        }
        return false;
    }

    public function setErrResponse($ket) {
        $resp = array(
            "response_code" => "01",
            "description" => $ket,
            "result" => "",
        );
        header("Content-Type: application/json");
        return json_encode($resp);
    }

    public function setSuccessResponse($result = "") {
        $resp = array(
            "response_code" => "01",
            "description" => "SUCCESS",
            "result" => $result
        );
        header("Content-Type: application/json");
        return json_encode($resp);
    }
}

?>