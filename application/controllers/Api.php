<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
  ===========================================================
  ::  => Author       : Robby Adnan F.
  => Email        : bobbyadnan17@gmail.com
  => Description  : API BILLER Controllers
  ===========================================================
 */

class Api extends RAF_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        echo $this->setErrResponse("Unknow method");
    }

    public function inq() {
        $request = $this->data['request'];
        if (!empty($request)) {
            $id_produk = $request->id_produk;
            $model = $this->get_produk_model($id_produk);
            if (empty($model)) {
                echo $this->setErrResponse("Biller Processor not found");
            } else {
                $this->load->model('biller/' . $model);
                echo $this->$model->process_inq($request);
            }
        } else {
            echo $this->setErrResponse("Empty request");
        }
    }

    public function pay() {
        $request = $this->data['request'];
        if (!empty($request)) {
            $id_produk = $request->id_produk;
            $model = $this->get_produk_model($id_produk);
            if (empty($model)) {
                echo $this->setErrResponse("Biller Processor not found");
            } else {
                $this->load->model('biller/' . $model);
                echo $this->$model->process_pay($request);
            }
        } else {
            echo $this->setErrResponse("Empty request");
        }
    }

}
