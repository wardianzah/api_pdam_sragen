<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pdam_sragen extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function process_inq($data) {
        $resp['result'] = array();
        $request = $this->db->query("CALL ViewRekNunggak(?)", array($data->id_pelanggan));
        $error = $this->db->error();

        log_message("error", "error: " . json_encode($error));
        
        if (!$error['code']) {
            $array = $request->result_array();
            log_message("error", "result: " . json_encode($array));
            
            $row = $request->row_array();
            $keys = array_keys($row);
            if (array_filter(array_keys($row), function ($key) {
                        return strpos($key, "STATUS") !== false;
                    })) {
                $resp["response_code"] = str_pad(preg_replace('/\D/', "", $keys[0]), 2, "0", STR_PAD_LEFT);
                $resp["description"] = $row[$keys[0]] . " Harap hubungi atau melakukan pembayaran di kantor PDAM Sragen terdekat";
            } else {
                $resp["response_code"] = "00";
                $resp["description"] = "SUCCESS";
                $resp["result"] = $array;
            }
        } else {
            $resp["response_code"] = "01";
            $resp["description"] = $error['message'];
        }

        return json_encode($resp);
    }

    public function process_pay($data) {
        $success = true;
        foreach ($data->periode as $periode) {
            log_message("error", sprintf("CALL BayarRekNunggak(%s, %s, %s, %s, %s)", $data->id_pelanggan, $periode, $data->id_loket_bayar, $data->operator, $data->person));
            $request = $this->db->query("CALL BayarRekNunggak(?, ?, ?, ?, ?)", array(
                $data->id_pelanggan,
                $periode,
                $data->id_loket_bayar,
                $data->operator,
                $data->person
            ));
            $error = $this->db->error();
            if ($error['code']) {
                $row = $request->row_array();
                log_message("error", "result  : " . json_encode($row));
                if (!isset($row['STATUS-8'])) {
                    $success = false;
                }
            }
            mysqli_next_result($this->db->conn_id);
        }

        return json_encode([
            "response_code" => $success ? "00" : "01",
            "description" => $success ? "SUCCESS" : "Terdapat tagihan yang gagal terbayar.",
            "result" => ""
        ]);
    }

}
